///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author @Daniel Luong<dluong@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @February 13 2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu(  bool isNative, enum Color newColor, enum Gender newGender ){
   gender = newGender;

   species = "Humu";

   scaleColor = newColor;

   favoriteTemp = 145.2;
   
   //Only Humuhumu uses the isNative changing bool to String
    native = Animal::bool2string(isNative);


}

const string Nunu::speak() {
   return Fish::speak();
}

//Print out Fish name and information

   void Nunu::printInfo(){
      cout << "Fish is Native =[" << native << "]"  << endl;
      Fish::printInfo();
   }

} //namespace animalfarm
